FROM tjaart/android

MAINTAINER Tjaart van der Walt <docker@tjaart.co.za>

ENV DEBIAN_FRONTEND noninteractive
# Install dependencies

RUN apt-get update && \
    apt-get install -qqy \
                    bash \
                    curl \
                    docker \
                    gettext \
                    git \
                    imagemagick \
                    openssl && \
    curl -sL https://deb.nodesource.com/setup_6.x | bash - && \
    apt-get install -qqy nodejs && \

    npm install -g cordova@6.3.1 && \
    npm install -g cordova-icon@0.9.0 && \
    npm install -g cordova-splash@0.9.0 && \
    npm install -g jspm@0.17.0-beta.29

ENV DOCKER_BUCKET get.docker.com
ENV DOCKER_VERSION 1.12.2
ENV DOCKER_SHA256 cb30ad9864f37512c50db725c14a22c3f55028949e4f1e4e585a6b3c624c4b0e
ENV PATH /usr/local/bin:$PATH

RUN set -x \
	&& curl -fSL "https://${DOCKER_BUCKET}/builds/Linux/x86_64/docker-${DOCKER_VERSION}.tgz" -o docker.tgz \
	&& echo "${DOCKER_SHA256} *docker.tgz" | sha256sum -c - \
	&& tar -xzvf docker.tgz \
	&& mv docker/* /usr/local/bin/ \
	&& rmdir docker \
	&& rm docker.tgz \
	&& docker -v

COPY docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["sh"]
